//
//  Profile.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/15/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import RealmSwift

class Profile: Object {
    @objc dynamic var name: String?
    @objc dynamic var email: String?
}
