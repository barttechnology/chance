//
//  LoginViewController.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/12/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import RealmSwift

struct MyProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        var name: String?
        var email: String?
        init(rawResponse: Any?) {
            // Decode JSON from rawResponse into other properties here.
            if let result = rawResponse as? [String:String] {
                if let name = result["name"] {
                    self.name = name
                }
                if let email = result["email"] {
                    self.email = email
                }
            }
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields": "id, name, email"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}

//import NetworkAPI

final internal class LoginViewController: UIViewController {
    
    var realm: Realm?
    
    @IBOutlet var loginView: LoginView! {
        didSet {
            loginView.delegate = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loginView?.setLoading(true)
        realm = try! Realm()
        title = "Login"
        
        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.
            print("Access Token: \(accessToken)")
            if !fetchProfile() {
                profileRequest()
            }
            loginComplete()
        } else {
            let loginButton = LoginButton(readPermissions: [ .publicProfile, .email ])
            loginButton.center = view.center
            loginButton.delegate = self
            view.addSubview(loginButton)
        }
        
        loginView?.setLoading(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
        
    }
    
    func fetchProfile() -> Bool {
        if let realm = self.realm {
            let profile = realm.objects(Profile.self).first
            if let _ = profile?.name, let _ = profile?.email {
                return true
            }
        }
        return false
    }
    
    func profileRequest() {
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                print("Custom Graph Request Succeeded: \(response)")

                let profile = Profile()
                profile.name = response.name ?? "Unknown user"
                profile.email = response.email ?? "Unknown email"
                
                if let realm = self.realm {
                    do {
                        try realm.write {
                            realm.add(profile)
                        }
                    } catch {
                        print("Error writing profile to realm")
                    }
                }
                
            case .failed(let error):
                print("Custom Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
    
    func loginComplete() {
        let homeViewController = HomeBuilder.make()
        show(homeViewController, sender: nil)
    }
}

extension LoginViewController: LoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("logged out")
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        profileRequest()
        loginComplete()
    }
}

extension LoginViewController: LoginViewDelegate {
    
}


// TODO: Customize facebook login button
// Reference below code
/*
func viewDidLoad() {
    // Add a custom login button to your app
    let myLoginButton = UIButton(type: .Custom)]
    myLoginButton.backgroundColor = UIColor.darkGrayColor()
    myLoginButton.frame = CGRect(0, 0, 180, 40);
    myLoginButton.center = view.center;
    myLoginButton.setTitle("My Login Button" forState: .Normal)
    
    // Handle clicks on the button
    myLoginButton.addTarget(self, action: @selector(self.loginButtonClicked) forControlEvents: .TouchUpInside)
    
    // Add the button to the view
    view.addSubview(myLoginButton)
}

// Once the button is clicked, show the login dialog
@objc func loginButtonClicked() {
    let loginManager = LoginManager()
    loginManager.logIn([ .publicProfile ], viewController: self) { loginResult in
        switch loginResult {
        case .Failed(let error):
            print(error)
        case .Cancelled:
            print("User cancelled login.")
        case .Success(let grantedPermissions, let declinedPermissions, let accessToken):
            print("Logged in!")
        }
}
*/
