//
//  LoginBuilder.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/12/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit

final class LoginBuilder {
    
    static func make() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        // TODO: bind network services
        //viewController.service = app.service()
        return viewController
    }
}
