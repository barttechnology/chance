//
//  LoginView.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/12/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit
import RSLoadingView

final class LoginView: UIView {
    weak var delegate: LoginViewDelegate?
}

extension LoginView: LoginViewProtocol {
    func setLoading(_ isLoading: Bool) {
        if isLoading {
            let loadingView = RSLoadingView()
            loadingView.show(on: self)
        } else {
            RSLoadingView.hide(from: self)
        }
    }
}
