//
//  LoginContracts.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/12/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import Foundation

@objc protocol LoginViewProtocol: class {
    var delegate: LoginViewDelegate? { get set }
    func setLoading(_ isLoading: Bool)
}

@objc protocol LoginViewDelegate: class {
    
}
