//
//  HomeBuilder.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/15/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit

final class HomeBuilder {
    
    static func make() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        return viewController
    }
}
