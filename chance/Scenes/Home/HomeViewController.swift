//
//  HomeViewController.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/15/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit
import RealmSwift

final internal class HomeViewController: UIViewController {
    
    @IBOutlet var homeView: HomeView! {
        didSet {
            homeView.delegate = self
        }
    }
    
    private var profile: Profile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Chance"
        
        let realm = try! Realm()
        if let profile = realm.objects(Profile.self).first {
            self.profile = profile
            if let name = profile.name {
                homeView.setName(name)
            }
        }
    }
}

extension HomeViewController: HomeViewDelegate {
    func showRewardVideo() {
        let adViewController = AdBuilder.make()
        show(adViewController, sender: nil)
    }
}
