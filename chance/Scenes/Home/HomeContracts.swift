//
//  HomeContracts.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/15/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import Foundation

@objc protocol HomeViewProtocol: class {
    var delegate: HomeViewDelegate? { get set }
    func setLoading(_ isLoading: Bool)
    func setName(_ name: String)
}

@objc protocol HomeViewDelegate: class {
    func showRewardVideo()
}
