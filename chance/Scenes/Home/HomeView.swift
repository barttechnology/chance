//
//  HomeView.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/15/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit
import RSLoadingView

final class HomeView: UIView {
    weak var delegate: HomeViewDelegate?
    @IBOutlet weak var nameLabel: UILabel!
    @IBAction func watchButtonPressed(_ sender: Any) {
        delegate?.showRewardVideo()
    }
}

extension HomeView: HomeViewProtocol {
    func setLoading(_ isLoading: Bool) {
        if isLoading {
            let loadingView = RSLoadingView()
            loadingView.show(on: self)
        } else {
            RSLoadingView.hide(from: self)
        }
    }
    
    func setName(_ name: String) {
        nameLabel.text = name
    }
}
