//
//  AdContracts.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/16/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import Foundation

@objc protocol AdViewProtocol: class {
    var delegate:AdViewDelegate? { get set }
    func setLoading(_ isLoading: Bool)
}

@objc protocol AdViewDelegate: class {
    func showVideo()
}
