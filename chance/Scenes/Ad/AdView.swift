//
//  AdView.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/16/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit
import RSLoadingView

final class AdView: UIView {
    weak var delegate: AdViewDelegate?
    @IBAction func watchButtonPressed(_ sender: Any) {
        delegate?.showVideo()
    }
}

extension AdView: AdViewProtocol {
    func setLoading(_ isLoading: Bool) {
        if isLoading {
            let loadingView = RSLoadingView()
            loadingView.show(on: self)
        } else {
            RSLoadingView.hide(from: self)
        }
    }
}

