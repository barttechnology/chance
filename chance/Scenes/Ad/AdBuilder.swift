//
//  AdBuilder.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/16/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit

final class AdBuilder {
    
    static func make() -> AdViewController {
        let storyboard = UIStoryboard(name: "Ad", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AdViewController") as! AdViewController
        return viewController
    }
}
