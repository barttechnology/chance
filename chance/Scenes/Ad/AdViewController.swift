//
//  AdViewController.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/16/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit
import GoogleMobileAds
import RealmSwift

let AD_UNIT_ID = "ca-app-pub-6649106864172173/2814542476"

class AdViewController: UIViewController {
    
    var ticket: Ticket?
    var realm: Realm?
    @IBOutlet var adView: AdView! {
        didSet {
            adView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        realm = try! Realm()
        fetchTicket()
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        GADRewardBasedVideoAd.sharedInstance().load(request,
                                                    withAdUnitID: AD_UNIT_ID)
        adView.setLoading(true)
    }
    
    func fetchTicket() {
        if let realm = realm {
            ticket = realm.objects(Ticket.self).first
        }
    }
    
    func createTicket() {
        if let realm = realm {
            do {
                ticket = Ticket()
                ticket!.counter = 1
                try realm.write {
                    realm.add(ticket!)
                }
            } catch {
                print("Error initializing ticket")
            }
        }
    }
    
    func updateTicket(counter newCounter: Int) {
        if let realm = realm {
            do {
                try realm.write {
                    ticket?.counter = newCounter
                }
            } catch {
                print("Error saving ticket to realm.")
            }
        }
    }
}

extension AdViewController: AdViewDelegate {
    func showVideo() {
        if GADRewardBasedVideoAd.sharedInstance().isReady == true {
            GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
        }
    }
}

extension AdViewController: GADRewardBasedVideoAdDelegate {
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
        fetchTicket()
        if let counter = ticket?.counter {
            print("current ad counter \(counter)")
            updateTicket(counter: counter+1)
        } else {
            print("creating a new ticket")
            createTicket()
        }
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        adView.setLoading(true)
        // reload ad
        // GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: AD_UNIT_ID)
        dismiss(animated: true, completion: nil)
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        print("Reward based video ad is received.")
        // setLoading false
        adView.setLoading(false)
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidCompletePlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad has completed.")
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
        // setLoading false
        // try again button
    }
    
}
