//
//  AppDelegate.swift
//  chance
//
//  Created by Ömer Mert Candan on 12/13/18.
//  Copyright © 2018 Bart Tech. All rights reserved.
//

import UIKit
import FacebookCore
import RealmSwift
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.configure(withApplicationID: "ca-app-pub-6649106864172173~6040182057")
        
        do {
            let _ = try Realm()
            print("Realm URL: \(Realm.Configuration.defaultConfiguration.fileURL!)")
        } catch {
            print("Error initializing new realm")
        }
        
        app.router.start()
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let appId: String = SDKSettings.appId
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)
        }
        return false
    }
}
